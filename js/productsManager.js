let productsResult;

let getProducts = function(){
    
    let url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
    let request = new XMLHttpRequest();

    request.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            //console.table(JSON.parse(request.responseText).value);
            productsResult = request.responseText;

            evalProducts();
        }
    }

    request.open("GET", url, true);

    request.send();
}

let evalProducts = function(){
    let products = JSON.parse(productsResult).value;

    let divContainer = document.getElementById("divProductsTable");

    divContainer.innerHTML = "";

    let divTable = document.createElement("div");

    divTable.classList.add("table");

    divTable.classList.add("table-bordered");

    divTable.classList.add("table-dark");
    //alert(products[0]);

    for (let index = 0; index < products.length; index++) {
        
        const ProductName = products[index].ProductName;

        const UnitPrice = products[index].UnitPrice;

        const UnitsInStock = products[index].UnitsInStock;
        
        let row = document.createElement("div");

        row.classList.add("row");

        let col1 = document.createElement("div");

        col1.classList.add("col");

        let col2 = document.createElement("div");

        col2.classList.add("col");

        let col3 = document.createElement("div");

        col3.classList.add("col");

        let span1 = document.createElement("span");

        span1.innerText = ProductName;

        let span2 = document.createElement("span");

        span2.innerText = UnitPrice;
        
        let span3 = document.createElement("span");

        span3.innerText = UnitsInStock;        

        col1.appendChild(span1);

        col2.appendChild(span2);

        col3.appendChild(span3);

        row.appendChild(col1);

        row.appendChild(col2);

        row.appendChild(col3);

        divTable.appendChild(row);

        //console.log(element);
    }

    divContainer.appendChild(divTable);
}