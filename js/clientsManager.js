
let clientsResult;

let getClients = function(){
    
    let filterval = txtFilter.value!=""?"?$filter=Country eq '" + txtFilter.value + "'": "";

    let url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers" +  filterval ;
    let request = new XMLHttpRequest();

    request.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            //console.table(JSON.parse(request.responseText).value);
            clientsResult = request.responseText;

            evalClients();
        }
    }

    request.open("GET", url, true);

    request.send();
}

let evalClients = function(){
    let clients = JSON.parse(clientsResult).value;

    let divContainer = document.getElementById("divClientsTable");

    divContainer.innerHTML = "";

    let divTable = document.createElement("div");

    divTable.classList.add("table");

    divTable.classList.add("table-bordered");

    divTable.classList.add("table-dark");
    //alert(products[0]);

    let flagPath = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

    for (let index = 0; index < clients.length; index++) {
        
        const ContactName = clients[index].ContactName;

        const City = clients[index].City;

        const Country = clients[index].Country;
        
        let row = document.createElement("div");

        row.classList.add("row");

        let col1 = document.createElement("div");

        col1.classList.add("col");

        let col2 = document.createElement("div");

        col2.classList.add("col");

        let col3 = document.createElement("div");

        col3.classList.add("col");

        let span1 = document.createElement("span");

        span1.innerText = ContactName;

        let span2 = document.createElement("span");

        span2.innerText = City;
        
        let img = document.createElement("img");

        img.src = flagPath + Country +".png";        

        img.style.width = "70px";

        img.style.height = "40px";

        col1.appendChild(span1);

        col2.appendChild(span2);

        col3.appendChild(img);

        row.appendChild(col1);

        row.appendChild(col2);

        row.appendChild(col3);

        divTable.appendChild(row);

        //console.log(element);
    }

    divContainer.appendChild(divTable);
}